#!/bin/bash
if [ -n "$CODER" ]; then
    DOTFILE_PATH=$HOME/.config/coderv2/dotfiles
else
    DOTFILE_PATH=$HOME/.dotfiles
    mv $HOME/.gitconfig $HOME/.gitconfig.bak
    cp $DOTFILE_PATH/hooks.sh /workspace/remote-dev/hooks.sh
fi

ln -s $DOTFILE_PATH/.gitconfig $HOME/.gitconfig
ln -s $DOTFILE_PATH/.bash_aliases $HOME/.bash_aliases

cp $DOTFILE_PATH/hooks.sh /workspace/remote-dev/hooks.sh
echo "source $DOTFILE_PATH/dbt.env" >> $HOME/.bashrc
# echo "git config --global core.filemode false" >> $HOME/.config/coderv2/
echo "git config core.fileMode false"  >> $HOME/.bashrc
chmod -R 777 ~/.aws

echo "source $DOTFILE_PATH/datapipeline.env" >> $HOME/.bashrc

export EDITOR=code
